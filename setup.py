#!/usr/bin/python
import sys
import shutil
import os
from distutils.dir_util import copy_tree

CURRENT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
EXECUTABLE_PATH = "/usr/share/gpio_manager"
CONFIG_PATH = "/etc/gpio_manager"
INIT_SCRIPT = os.path.join(CURRENT_DIR, "gpio_manager/setup/gpio_manager")


def install():
    if not os.path.exists(EXECUTABLE_PATH):
        os.makedirs(EXECUTABLE_PATH)
    if not os.path.exists(CONFIG_PATH):
        os.makedirs(CONFIG_PATH)

    # Copying python files to executable path
    for file_name in os.listdir(os.path.join(CURRENT_DIR, 'gpio_manager')):
        if file_name.endswith('.py'):
            shutil.copy2(os.path.join(CURRENT_DIR, 'gpio_manager', file_name), EXECUTABLE_PATH)

    # Config json to config path
    shutil.copy2(os.path.join(CURRENT_DIR, 'gpio_manager/config.json'), CONFIG_PATH)
    # Default scripts to config path
    copy_tree(os.path.join(CURRENT_DIR, 'gpio_manager/scripts'), os.path.join(CONFIG_PATH, 'scripts'))
    # Adding the startup script
    shutil.copy2(INIT_SCRIPT, '/etc/init.d/')
    # Creating the message file
    msg_file = open(os.path.join(CONFIG_PATH, 'messages'), 'w+')
    msg_file.close()


def uninstall():
    shutil.rmtree(EXECUTABLE_PATH)
    shutil.rmtree(CONFIG_PATH)
    os.remove('/etc/init.d/gpio_manager')


def main():
    if len(sys.argv) != 2:
        sys.exit('Syntax: %s COMMAND (install|uninstall|remove)' % sys.argv[0])

    if not os.geteuid() == 0:
        sys.exit('Script must be run as root')

    cmd = sys.argv[1].lower()

    if cmd == "install":
        install()
    elif cmd == "uninstall" or cmd == "remove":
        uninstall()
    elif cmd == 'reinstall':
        uninstall()
        install()
    else:
        sys.exit('Unknown command "%s".' % cmd)


if __name__ == "__main__":
    main()