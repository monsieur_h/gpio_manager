import subprocess
import shlex


class Behavior(object):
    def __init__(self, **kwargs):
        self.state = None

    def update(self, deltatime):
        raise NotImplementedError()


class BlinkingBehavior(Behavior):
    def __init__(self, **kwargs):
        super(BlinkingBehavior, self).__init__()
        self.state = True

    def update(self, deltatime):
        self.state = not self.state


class DefaultBehavior(Behavior):
    def __init__(self, **kwargs):
        super(DefaultBehavior, self).__init__()
        self.state = True

    def update(self, deltatime):
        pass


class ProgramLauncherBehavior(Behavior):
    def __init__(self, **kwargs):
        super(ProgramLauncherBehavior, self).__init__()
        self.program = kwargs["program"]
        self.pid = None

    def update(self, deltatime):
        if self.state:
            self.state = False
            self.toggle()

    def _start_program(self):
        self.pid = subprocess.Popen(shlex.split(self.program))

    def _stop_program(self):
        self.pid.terminate()
        self.pid = None

    def toggle(self):
        if self.pid:
            self._stop_program()
        else:
            self._start_program()


class BehaviorFactory(object):
    @staticmethod
    def create(name, **kwargs):
        if name == "blink":
            return BlinkingBehavior(**kwargs)
        elif name == "trigger":
            return ProgramLauncherBehavior(**kwargs)
        else:
            return DefaultBehavior(**kwargs)


class ChainBehavior(object):
    def __init__(self, **kwargs):
        self._behavior = []
        for behavior in kwargs:
            self._behavior.append(behavior)

    def update(self, deltatime):
        for beh in self._behavior:
            beh.update(deltatime)


class MessageFactory(object):
    @staticmethod
    def create(creation_dict):
        return Message(**creation_dict)

    @staticmethod
    def messages_from_file(file_path):
        messages = []
        kept_line = []
        with open(file_path) as file_handle:
            for line in file_handle.readlines():
                if line.startswith('#'):
                    kept_line.append(line)
                    continue
                new_msg = MessageFactory.create(MessageFactory.split_arguments(line))
                messages.append(new_msg)

        with open(file_path, 'w+') as file_handle:
            file_handle.writelines(kept_line)

        return messages

    @staticmethod
    def split_arguments(line):
        """
        Returns a dictionary for a line in format key1:value, key2:value...
        TODO: Use a standard filetype
        """
        argument_list = {}
        pairs = line.split(',')
        for pair in pairs:
            args = pair.split(':')
            argument_list[args[0].strip()] = args[1].strip()
        return argument_list


class Message(object):
    """
    A message to the program
    """

    def __init__(self, **args):
        for name in args:
            setattr(self, name, args[name])