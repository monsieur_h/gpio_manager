#!/usr/bin/python

import time

from service import Service

from util import JSONConfigLoaderStrategy
from hardware import PinFactory
from behaviors import MessageFactory, BehaviorFactory


class Clock(object):
    """
    A simple clock to measure time between two events
    """

    def __init__(self):
        self._last_update = 0

    def tick(self):
        """
        Ticks the clock : updates the last update time and return the elapsed time since the last tick/update
        """
        elapsed = time.time() - self._last_update
        self.update()
        return elapsed

    def update(self):
        self._last_update = time.time()


class Application(Service):
    CONFIG_FILE_PATH = "/etc/gpio_manager/config.json"

    def __init__(self, name, sleep_time=1, load_strategy=JSONConfigLoaderStrategy,
                 message_file="/etc/gpio_manager/messages", pid_dir="/tmp"):
        super(Application, self).__init__(name, pid_dir=pid_dir)
        self.clock = Clock()
        self.sleep = sleep_time
        self.message_file = message_file
        self._inputs = []
        self._outputs = []
        self.pin_factory = None
        self._load_strategy = load_strategy

    def warmup(self):
        self.logger.info("Starting...")
        self.logger.info("Reading config file...")
        self._load_config(self._load_strategy)
        self.logger.info("Creating factory...")
        self.pin_factory = PinFactory()
        self.logger.info("Booting GPIO...")
        self._create_pins()

    def cleanup(self):
        self.logger.info("Stopping...")
        if self.pin_factory:
            self.pin_factory.cleanup()

    def _load_config(self, strategy):
        self._config = strategy.load_file(Application.CONFIG_FILE_PATH)

    def _create_pins(self):
        for pin in self._config:
            gpio_mode = (pin["type"] == "output")
            channel = pin.pop("channel")
            new_pin = self.pin_factory.create(channel, gpio_mode, **pin)
            new_pin.name = pin["name"]
            if gpio_mode:
                self._outputs.append(new_pin)
            else:
                self._inputs.append(new_pin)

    def update(self, deltatime):
        self.read_messages()
        self.poll_inputs(deltatime)
        self.update_outputs(deltatime)

    def read_messages(self):
        messages = MessageFactory.messages_from_file(self.message_file)
        for message in messages:
            if message.type == "change_behavior":
                self.logger.info(
                    "Received 'change_behavior' message for {} ({})".format(message.target, message.behavior))
                pin = self.pin_factory.get(int(message.target))
                pin.behavior = BehaviorFactory.create(message.behavior)

    def poll_inputs(self, dt):
        for inp in self._inputs:
            inp.update(dt)

    def update_outputs(self, dt):
        for out in self._outputs:
            out.update(dt)

    def run(self):  # Main loop here
        self.warmup()
        self.clock.update()
        while not self.got_sigterm():
            dt = self.clock.tick()
            self.update(dt)
            time.sleep(self.sleep)
        self.cleanup()


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 2:
        sys.exit('Syntax: %s COMMAND' % sys.argv[0])

    cmd = sys.argv[1].lower()

    service = Application('gpio_service', pid_dir='/tmp', load_strategy=JSONConfigLoaderStrategy)

    if cmd == 'start':
        service.start()
    elif cmd == 'stop':
        service.stop()
    elif cmd == 'restart':
        service.stop()
        time.sleep(1)
        service.start()
    elif cmd == 'status':
        if service.is_running():
            print "Service %s is running." % service.name
        else:
            print "Service %s is not running." % service.name
    else:
        sys.exit('Unknown command "%s".' % cmd)