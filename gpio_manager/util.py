import json
import os


class Dispatcher(object):
    def __init__(self):
        self._listeners = []

    def register(self, callable_listener):
        self._listeners.append(callable_listener)

    def un_register(self, callable_listener):
        self._listeners.remove(callable_listener)

    def dispatch(self, *args):
        for listener in self._listeners:
            listener(*args)


class Observable(object):
    """
    Observable class contains a dispatcher that relays events to observer classes
    """

    def __init__(self):
        self.event_dispatcher = Dispatcher()


class ConfigLoaderStrategy(object):
    @staticmethod
    def load_file(file_path):
        raise NotImplementedError()


class JSONConfigLoaderStrategy(ConfigLoaderStrategy):
    @staticmethod
    def load_file(file_path):
        if os.path.exists(file_path):
            return json.load(open(file_path))