try:
    import RPi.GPIO as GPIO
except RuntimeError:
    print(
        "Error importing RPi.GPIO!  This is probably because you need superuser privileges. "
        "You can achieve this by using 'sudo' to run your script")

GPIO.setmode(GPIO.BOARD)  # always
from behaviors import BehaviorFactory
from util import Observable


class Pin(Observable):
    """
    Represents a GPIO pin
    # TODO: Refactor in two classes : OUTPUT and INPUT
    """

    def __init__(self, channel, **kwargs):
        super(Pin, self).__init__()
        self._channel = channel
        self.name = "Unnamed pin channel %d" % self._channel
        self.channel = self._channel  # Readonly
        self._state = False
        if kwargs["behavior"]:
            self.behavior = BehaviorFactory.create(**kwargs["behavior"])

        self.event_dispatcher.dispatch(self.__class__, "INIT")

    def _acquire_lock(self):
        raise NotImplemented()

    def _invalid_mode(self):
        raise InvalidModeException("Invalid mode for channel %d" % self._channel)

    def __del__(self):
        GPIO.cleanup(self._channel)

    def update(self, deltatime):
        if self.behavior:
            self.behavior.update(deltatime)


class OutputPin(Pin):
    def __init__(self, channel, **kwargs):
        super(OutputPin, self).__init__(channel, **kwargs)
        self._mode = GPIO.OUT
        self.mode = "OUTPUT"
        self._acquire_lock()

    def update(self, deltatime):
        super(OutputPin, self).update(deltatime)
        self.state = self.behavior.state

    def _get_state(self):
        return self._state

    def _set_state(self, state):
        self._state = state
        GPIO.output(self._channel, self._state)

    def _acquire_lock(self):
        GPIO.setup(self._channel, self._mode)

    state = property(_get_state, _set_state)


class InputPin(Pin):
    def __init__(self, channel, **kwargs):
        super(InputPin, self).__init__(channel, **kwargs)
        self._mode = GPIO.IN
        self.mode = "INPUT"
        self._acquire_lock()

    def update(self, deltatime):
        self.read_state()
        self.behavior.state = self.state
        super(InputPin, self).update(deltatime)

    def read_state(self):
        self._state = not GPIO.input(self._channel)

    def _get_state(self):
        return self._state

    def _set_state(self, state):
        super(InputPin, self)._invalid_mode()

    def _acquire_lock(self):
        GPIO.setup(self._channel, self._mode, pull_up_down=GPIO.PUD_UP)

    state = property(_get_state, _set_state)


class ChannelAlreadyUsedException(Exception):
    pass


class InvalidModeException(Exception):
    pass


class PinFactory(object):
    def __init__(self):
        self._used_channels = []
        self._pin = {}

    def create(self, channel, output, **kwargs):
        if channel in self._used_channels:
            raise ChannelAlreadyUsedException("Channel %d is already used" % channel)
        self._used_channels.append(channel)
        if output:
            self._pin[channel] = OutputPin(channel, **kwargs)
        else:
            self._pin[channel] = InputPin(channel, **kwargs)
        return self._pin[channel]

    def get(self, channel):
        if channel in self._pin.keys():
            return self._pin[channel]

    def cleanup(self):
        GPIO.cleanup()

class LED(Pin):
    def __init__(self, channel):
        super(LED, self).__init__(channel)

    def activate(self):
        GPIO.output(self._channel, GPIO.HIGH)

    def deactivate(self):
        GPIO.output(self._channel, GPIO.LOW)

    def set(self, activation):
        if activation:
            self.activate()
        else:
            self.deactivate()


class Button(Pin):
    def __init__(self, channel):
        super(Button, self).__init__(channel)

    def is_pressed(self):
        return GPIO.input(self._channel)


class LightButton(Observable):
    def __init__(self, button, led):
        super(LightButton, self).__init__()
        self.button = button
        self.led = led