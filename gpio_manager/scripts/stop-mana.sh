#!/bin/bash

pkill hostapd
rm /tmp/crackapd.run
rm $EXNODE
pkill dhcpd
pkill dnsspoof
pkill tinyproxy
pkill stunnel4
pkill msfconsole
service apache2 stop
iptables -t nat -F
