#!/bin/bash
# Toggles the mana script

TOGGLE_FILE="/etc/gpio_manager/scripts/mana_pid"
PROGRAM="/etc/gpio_manager/scripts/"
MESSAGE_FILE="/etc/gpio_manager/messages"

if [[ -f ${TOGGLE_FILE} ]]; then
	echo "Shutdown"
	echo "target:11, behavior: blink, type: change_behavior" > ${MESSAGE_FILE}
	rm ${TOGGLE_FILE}
	PROGRAM="${PROGRAM}stop-mana.sh"
	bash ${PROGRAM}&
else
	echo "Launch"
	echo "target:11, behavior: default, type: change_behavior" > ${MESSAGE_FILE}
	bash ${PROGRAM} &
	echo $! > ${TOGGLE_FILE}
	PROGRAM="${PROGRAM}start-mana.sh"
	bash ${PROGRAM}&
fi
